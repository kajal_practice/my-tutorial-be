# Use the official Node.js image as the base image
FROM node:18

# Set the working directory inside the container
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install application dependencies
RUN npm install

# Copy the rest of the application source code to the container
COPY . .

# Expose the port on which your Express app listens (replace 3000 with your app's port if different)
EXPOSE 8080

# Define the command to start your Express.js application
CMD ["node", "server.js"]
