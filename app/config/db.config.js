module.exports = {
  HOST: "testdb.cjza1v85t2fv.us-east-1.rds.amazonaws.com",
  USER: "postgres",
  PASSWORD: "postgres",
  DB: "testdb",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
